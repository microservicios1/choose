<html lang="es">
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Solicitudes</title>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <?php
      include 'dbc.php';
      include 'session.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      if(! $conn )
        die('Conexion sql fallida!');
    ?>
    <style>
      th
      {
        font-size: 16px;
        border: 1px solid black;
        text-align: center;
      }
      td
      {
        text-align: center;
        font-size: 16px;
        border: 1px solid black;
      }
    </style>
  </head>
  <body>
    <div class="container" align="center">
      <!--     Navi     -->
      <ul id="nav">
        <li><a href="<?php echo $logout;?>">Cerrar sesion</a></li>
        <?php
          if($_COOKIE['userName']=='VY8G08A')
          {
            ?>
            <li><a href="<?php echo $consulk;?>">Spec Ops</a></li>
            <?php
          }
        ?>
        <li>User : <?php echo $_COOKIE['userName'];?></li>
        <!--<li><?php //echo "<script>var w = screen.width-60;var h=screen.height-140</script>"; echo "<a href=\"#\" onclick=\"window.open('".$showtables."','','menubar=0,titlebar=0,width='+w+',height='+h+',resizable=0,left=60px,top=40px')\" >Mostrar historial</a>";?></li>-->
        <li><a href="<?php echo $solicitudes;?>">Crear Solicitud</a></li>
        <li><a href="<?php echo $reporte;?>">Reportes</a></li>
        <li><a href="<?php echo $choose;?>">Solicitudes Actuales</a></li>
        <li clas="current"><a href="<?php echo $inside;?>">Proyectos</a></li>
      </ul>
      <br><br>
      <form method='post' action='cc.php' id='fist' >
        <br><br><br><br>
          Direccion: <select name="Direccion" id="Direccion" onchange="this.form.submit()" >
            <option value="">- Direccion -</option>
            <?php
              $conn = mysqli_connect($host, $user, $pass, $db);
              $re = mysqli_query($conn,"select direcciones.nombre,direcciones.direccionId,count(solicitudes.direccionId) from direcciones left join solicitudes on direcciones.direccionId=solicitudes.direccionId group by direcciones.direccionId");
              $r=mysqli_affected_rows($conn);
              if($r<1)
                echo "<option value=\"\">No disponible</option> ";
              else
              while($row = mysqli_fetch_array($re))
              {
                $o ="<option ";
                if($row[1]==$_POST['Direccion'])
                  $o .= "selected ";
                $o .= "value=\"".$row[1]."\">".$row[0]." solicitudes: ".$row[2]."</option>";
                echo $o;
              }
            ?>
          </select>
      </form>
      <br><br><br><br>
      <table width="60%">
      <tr>
        <th>Solicitudes</th>
      </tr>
      <?php
        if($_POST['Direccion']!="")
        {
          $sql="select folio,fecha,proyecto,F60 from solicitudes where direccionId='".$_POST['Direccion']."'";
          $re = mysqli_query($conn,$sql);
          $r=mysqli_affected_rows($conn);
          if($r<1)
            echo "<tr><td><br><br>Niniguna solicitud en direccion seleccionada;</tr></td>";
          else
          while($row = mysqli_fetch_array($re))
          {
            if($row['F60']==0)
              echo "<tr><td><br><a style=\"color:red;\" href='cleanfolio.php?folio=".$row['folio']."'>Proyecto: ".$row['proyecto']."  solicitado el ".$row['fecha']." Falta F60</a></tr></td>";
            else
              echo "<tr><td><br><a href='cleanfolio.php?folio=".$row['folio']."'>Proyecto: ".$row['proyecto']."  solicitado el ".$row['fecha']." </a></tr></td>";
          }
          mysqli_close($conn);
        }
      ?>      
      </table>
      <br><br><p>  </p><br>
    </div>
  </body>
</html>