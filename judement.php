<html>
<head>
  <title>Respuesta</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <?php
    include 'dbc.php';
    include 'session.php';
    $conn = mysqli_connect($host, $user, $pass, $db);
    if(! $conn )
        die('Conexion sql fallida!');
  ?>
</head>
<body>
  <div class="container" align="center">
    <br>
    <!--     Navi     -->
      <ul id="nav">
        <li><a href="<?php echo $logout;?>">Cerrar sesion</a></li>
        <?php
          if($_COOKIE['userLvl']==1)
          {
            ?>
            <li><a href="<?php echo $consulk;?>">Spec Ops</a></li>
            <?php
          }
        ?>
        <li>User : <?php echo $_COOKIE['userName'];?></li>
        <li><?php echo "<script>var w = screen.width-60;var h=screen.height-140</script>"; echo "<a href=\"#\" onclick=\"window.open('".$showtables."','','menubar=0,titlebar=0,width='+w+',height='+h+',resizable=0,left=60px,top=40px')\" >Mostrar historial</a>";?></li>
        <li clas="current"><a href="<?php echo $choose;?>">Solicitudes</a></li>
        <li ><a href="<?php echo $inside;?>">Proyectos</a></li>
      </ul>
    <br>
    <?php
      if(isset($_POST['thisisnice']))
      {
        mysqli_autocommit($conn, FALSE);
        mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
        $sql="select diagrama,fileHardware,fileLicencias from solicitudes where folio='".$_POST['folio']."'";
        $re=mysqli_query($conn,$sql);
        $rowOfMaps = mysqli_fetch_array($re);
        $controlVariableSome=0;
        $time= new DateTime();
        $time=$time->format('Y-m-d');
        $newFolio=substr($_POST['proyecto'],0,15)."_".$time."_".$_COOKIE['userName'];
        //   definir sql de proyectos 
          $sql = "insert into proyectos values ('".$newFolio."','".$time."',".$_POST['direccion'].",'".$_POST['proyecto']."','".$rowOfMaps['diagrama']."','".$_POST['comentarios']."','".$_POST['descripcion']."','".$_POST['solicitudPedido']."',";
          if(is_numeric($_POST['inversion']))
            $sql .= $_POST['inversion'].",'";
          else
            $sql .= "0,'";  
          $sql .= $_POST['moneda']."',";
          if($_POST['hardwareLiberado']=="on")
            $sql .= "1,'";
          else
            $sql .= "0,'";
          $sql .= $_POST['cantidadesHW']."','".$_POST['productoHW']."',";
          if(is_numeric($_POST['SOLPEHW']))
            $sql .= $_POST['SOLPEHW'].",'";
          else
            $sql .= "0,'";
          $sql .= $rowOfMaps['fileHardware']."',";
          if($_POST['licenciasLiberado']=="on")
            $sql .= "1,'";
          else
            $sql .= "0,'";
          $sql .= $_POST['cantidadesL']."','".$_POST['productoL']."',";
          if(is_numeric($_POST['SOLPEL']))
            $sql .= $_POST['SOLPEL'].",'";
          else
            $sql .= "0,'";
          $sql .= $rowOfMaps['fileLicencias']."','".$_POST['solicita']."','".$_POST['administra']."','".$_POST['criticidad']."','".$_POST['horario']."','".$_POST['nUser']."','".$_POST['ingresos']."','".$_POST['disponibilidad']."','".$_POST['gerencia']."','Proyecto solicitado por el usuario".$_POST['solicita']." el dia :".$_POST['fecha']." \\nAprobado para base interna el dia:".$time."\\n')";
        mysqli_query($conn,$sql);
        $r=mysqli_affected_rows($conn);      
        if($r<1)
        {
          echo '<script type="text/javascript">alert("El nombre de proyecto esta ocupado o hubo un error al conectar con la base de datos");</script>';
          $controlVariableSome=99;
        }
        else
        {
          $sql ="select * from filtroMaquinas where folio='".$_POST['folio']."'";
          $reOfMachines=mysqli_query($conn,$sql);
          $re=mysqli_affected_rows($conn);
          if($re<1)
            $controlVariableSome=99;
          else
          {
            while($rowMachines = mysqli_fetch_array($reOfMachines))
            {
              $dato=substr($rowMachines['interId'],strlen($rowMachines['folio'])); 
              $sql="insert into maquinas (folio,interId,arreglo,tipo,aplicacion,ambienteSolicitado,CPUSolicitado,RAMSolicitado,estatus,historico) values ('".$newFolio."','".$newFolio.$dato."',".$rowMachines['arreglo'].",'".$rowMachines['tipo']."','".$rowMachines['aplicacion']."','".$rowMachines['ambienteSolicitado']."',".$rowMachines['CPU'].",".$rowMachines['RAM'].",'PENDIENTE','Maquina Solicitada por el usuario el dia :".$_POST['fecha']." \\nAprobada para base interna el dia:".$time."')";
              mysqli_query($conn,$sql);
              $check=mysqli_affected_rows($conn);            
              if($check<1)
              {
                $controlVariableSome=99;
                echo $sql.";<br>";
              } 
              else
              {
                $sql="select nombreDisco,mountpoint,sizeDisco,tipoDisco,proposito,performance,tipo from filtroDisco where interId='".$rowMachines['interId']."'";
                $disks=mysqli_query($conn,$sql);
                $estatico=0;
                $shared=0;
                while($tempDisks=mysqli_fetch_array($disks))
                {
                  //  Añadir discos
                    $sql="insert into disco (interId,nombreDisco,mountpoint,sizeDiscoS,sizeDiscoE,tipoDisco,proposito,performance,tipo) values ('".$newFolio.$dato."','".$tempDisks['nombreDisco']."','".$tempDisks['mountpoint']."',".$tempDisks['sizeDisco'].",0,'".$tempDisks['tipoDisco']."','".$tempDisks['proposito']."','".$tempDisks['performance']."','".$tempDisks['tipo']."')";
                    mysqli_query($conn,$sql);                  
                    $check=mysqli_affected_rows($conn);
                    if($check<1)
                    {
                      $controlVariableSome=99;
                      echo $sql.";<br>";
                    }
                    if($tempDisks['tipo']=="Estatico")
                      $estatico += $tempDisks['sizeDisco'];
                    if($tempDisks['tipo']=="Compartido")
                      $shared += $tempDisks['sizeDisco'];
                }
                $sql="select IP,descripcion from filtroIP where interId='".$rowMachines['interId']."'";
                $IPS=mysqli_query($conn,$sql);
                $ipn=0;
                while($tempIPs=mysqli_fetch_array($IPS))
                {
                  //  Añadir discos
                    $sql="insert into interIP (interId,IP,descripcion) values ('".$newFolio.$dato."','".$tempIPs['IP']."','".$tempIPs['descripcion']."')";
                    mysqli_query($conn,$sql);                  
                    $check=mysqli_affected_rows($conn);
                    if($check<1)
                    {
                      $controlVariableSome=99;
                      echo $sql.";<br>";
                    }
                }
                $sql="update maquinas set maquinas.storageSolicitado=".$estatico.",maquinas.sharedSolicitado=".$shared." where interId='".$newFolio.$dato."'";
                mysqli_query($conn,$sql);               
                $check=mysqli_affected_rows($conn);
                if($check<1)
                {
                  $controlVariableSome=99;
                  echo $sql.";<br>";
                }
                else
                {
                  //    Agregar Info Especificaciones SO
                    $sql="select * from filtroEspecificacionesSO where folioNumber='".$rowMachines['interId']."'";
                    $re=mysqli_query($conn,$sql);
                    $check=mysqli_affected_rows($conn);
                    if($check<1)
                    {
                      if($rowMachines['tipo']!=2)
                      {
                        $controlVariableSome=99;
                        echo $sql.";<br>";                     
                      }
                    }
                    else
                    {
                      $rowSO = mysqli_fetch_array($re);
                      $sql="insert into especificacionesSO (folioNumber,folio,SOSolicitado,usuarios,filesystems,software,kernel,addicionalConfig) values ('".$newFolio.$dato."','".$rowSO['folio']."','".$rowSO['SO']."',".$rowSO['usuarios'].",".$rowSO['filesystems'].",".$rowSO['software'].",'".$rowSO['kernel']."','".$rowSO['addicionalConfig']."')";
                      mysqli_query($conn,$sql);                    
                      $check=mysqli_affected_rows($conn);
                      if($check<1)
                      {
                        $controlVariableSome=99;
                        echo $sql.";<br>";                     
                      }
                      else
                      {
                        if($rowSO['usuarios']>0)
                        {
                          $sql="select * from filtroSOUsuario where folioNumber='".$rowSO['folioNumber']."'";
                          $moveInfo=mysqli_query($conn,$sql);
                          while($users=mysqli_fetch_array($moveInfo))
                          {
                            $sql="insert into SOUsuario values ('".$newFolio.$dato."','".$users['username']."','".$users['grupo']."','".$users['home']."','".$users['shell']."','".$users['perfil']."','".$users['specialConfig']."')";
                            mysqli_query($conn,$sql);                          
                            $check=mysqli_affected_rows($conn);
                            if($check<1)
                            {
                              $controlVariableSome=99;
                              echo $sql.";<br>";
                            }                      
                          }
                        }
                        $sql="select * from filtroSOFilesystem where folioNumber='".$rowSO['folioNumber']."'";
                        $moveInfo=mysqli_query($conn,$sql);
                        while($fileSystems=mysqli_fetch_array($moveInfo))
                        {
                          $sql="insert into SOFilesystem values ('".$newFolio.$dato."','".$fileSystems['mountpoint']."',".$fileSystems['sizeGB'].",'".$fileSystems['propietario']."','".$fileSystems['grupo']."','".$fileSystems['permisos']."')";
                          mysqli_query($conn,$sql);                        
                          $check=mysqli_affected_rows($conn);
                          if($check<1)
                          {
                            $controlVariableSome=99;
                            echo $sql.";<br>";                     
                          } 
                        }
                        if($rowSO['software']>0)
                        {
                          $sql="select * from filtroSOSoftware where folioNumber='".$rowSO['folioNumber']."'";                        
                          $moveInfo=mysqli_query($conn,$sql);
                          while($software=mysqli_fetch_array($moveInfo))
                          {
                            $sql="insert into SOSoftware values ('".$newFolio.$dato."','".$software['producto']."','".$software['versionP']."')";
                            mysqli_query($conn,$sql);                          
                            $check=mysqli_affected_rows($conn);
                            if($check<1)
                            {
                              $controlVariableSome=99;
                              echo $sql.";<br>";                     
                            } 
                          }
                        }
                      }
                    }
                  //    Agregar Especificaciones a DB
                    if($controlVariableSome==0)
                    {
                      $sql="select * from filtroEspecificacionesDB where folioNumber='".$rowMachines['interId']."'";
                      $re=mysqli_query($conn,$sql);
                      $check=mysqli_affected_rows($conn);
                      if($check>0)
                      {
                        $rowDB = mysqli_fetch_array($re);
                        $sql="insert into especificacionesDB (folioNumber,folio,puerto,DBname,especificaciones,usuarios,response1,comentarios,base) values ('".$newFolio.$dato."','".$rowDB['folio']."',".$rowDB['puerto'].",'".$rowDB['DBname']."',".$rowDB['especificaciones'].",".$rowDB['usuarios'].",".$rowDB['response1'].",'".$rowDB['comentarios']."','".$rowDB['base']."')";
                        mysqli_query($conn,$sql);                      
                        $check=mysqli_affected_rows($conn);
                        if($check<1)
                        {
                          $controlVariableSome=99;
                          echo $sql.";<br>";                     
                        }
                        else
                        {
                          if($rowDB['especificaciones']>0)
                          {
                            $sql="select * from filtroDBEspecificaciones where folioNumber='".$rowDB['folioNumber']."'";
                            $moveInfo=mysqli_query($conn,$sql);
                            while($specs2=mysqli_fetch_array($moveInfo))
                            {
                              $sql="insert into DBEspecificaciones values ('".$newFolio.$dato."','".$specs2['especificacion']."','".$specs2['valor']."','".$specs2['valorDef']."')";
                              mysqli_query($conn,$sql);                            
                              $check=mysqli_affected_rows($conn);
                              if($check<1)
                              {
                                $controlVariableSome=99;
                                echo $sql.";<br>";                     
                              }
                            }
                          }
                          if($rowDB['usuarios']>0)
                          {
                            $sql="select * from filtroDBUsuarios where folioNumber='".$rowDB['folioNumber']."'";
                            $moveInfo=mysqli_query($conn,$sql);
                            while($usuario2=mysqli_fetch_array($moveInfo))
                            {
                              $sql="insert into DBUsuarios values ('".$newFolio.$dato."','".$usuario2['dbuName']."','".$usuario2['dbuRol']."','".$usuario2['dbuPrivilegios']."')";
                              mysqli_query($conn,$sql);                            
                              $check=mysqli_affected_rows($conn);
                              if($check<1)
                              {
                                $controlVariableSome=99;
                                echo $sql.";<br>";                     
                              }
                            }
                          }
                        }
                      }  
                    }
                }
              }
            }
          }
        }
        //  borrar todo de filtro 
          if($controlVariableSome==0)
          {
            $sql="delete from filtroEspecificacionesDB where folio='".$_POST['folio']."'";          
            mysqli_query($conn,$sql);
            $sql="delete from filtroEspecificacionesSO where folio='".$_POST['folio']."'";          
            mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
              $controlVariableSome=99;
            $sql="delete from filtroMaquinas where folio='".$_POST['folio']."'";          
            $re=mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
              $controlVariableSome=99;
            $sql="delete from solicitudes where folio='".$_POST['folio']."'";          
            $re=mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
              $controlVariableSome=99;
          }
        //  commit o rollback
          if($controlVariableSome==0)
          {
            echo "Datos agregados .";
            mysqli_commit($conn);
            mysqli_autocommit($conn, TRUE);
            $v=$rowOfMaps['diagrama'];
            rename ("the-other-images/fil/$v", "the-other-images/apr/$v");
            $v=$rowOfMaps['fileHardware'];
            if($v!="")
              rename ("the-other-images/solicitudHard/$v", "the-other-images/interHard/$v");
            @unlink("the-other-images/solicitudHard/".$v);
            $v=$rowOfMaps['fileLicencias'];
            if($v!="")
              rename ("the-other-images/solicitudLice/$v", "the-other-images/interLice/$v");
            @unlink("the-other-images/solicitudLice/".$v);
            
            echo "<iframe src=\"";
              print ("http://historico-ex379816-project-dev.apps.paas.telcelcloud.dt/someec.php");
            echo "\" width=\"0\" height=\"0\"></iframe>";
          }
          else
          {
            echo "Datos no agregados .Problema de conexion con base de datos!";
            mysqli_rollback($conn);
            mysqli_autocommit($conn, TRUE);
          }  
        mysqli_close($conn);
      }
    ?>
    <p>  </p><br>
  </div>
</body>
</html> 