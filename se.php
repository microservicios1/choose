<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Crecimiento de Proyecto</title>
    <style>
      body
      {
        background-image: url(all-of-those-images/interf/logoazul.png);
        background-repeat: no-repeat;
        background-size: 240px 70px;
        background-position: 6% 25px;
      }
      table, th, td
      {
        border: 1px solid black;
        text-align: center;
      }
      th
      {
        font-size: 16px;
        font-weight: bold;
      }
      .shad
      {
        font-size: 20px;
        font-weight: bold;
        border: 1px flat #000033;
      }
      button,input[type=submit],input[type=reset]
      {
        background-color: #D6EAF8;
        padding: 4px 4px;
        border: outset #ABB2B9;
        cursor: pointer;
        font-size: 15px;
        font-weight: bold;
        box-shadow: 2px 3px 10px #000033;
      }
      .evilbtn
      {
        display: inline;
        background-color: #F7DC6F;
        border:  double #FCF3CF;
        font-size: 16px;
        font-weight: bold;
        float: right;
        margin-right: 5%;
        cursor: default;
        border-radius: 50%;
        height: 70px;
      }
      .containerOfRod
      {
        padding: 4px 4px;
        box-sizing: border-box;
        font-size: 14px;
        border:10px groove #616161;
        border-radius: 10px;
      }
      .unselectable
      {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
    </style>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host,$user,$pass,$db);
      if($_POST['sola']=="")
        header('Location: http://'.$sols);
    ?>
  </head>
<body>
  <div class="containerOfRod">
    <form method="post"  action="judment2.php" > <br>
      <br>
      <div class="shad" align="center">
        <input type="submit" value="Aprobar Crecimiento" name="thisisnice" id="thisisnice" >
        <input type="submit" value="Rechazar Crecimiento" name="mercifullno" id="mercifullno">
        <input type="submit" value="Rechazar y eliminar" name="killandgo" id="killandgo">
      </div>
      <br><br>
      <h1 align="center">Solicitado</h1>
      <table align="center">
        <tr>
          <th>No:</th>
          <th width="20">vCPUs:</th>
          <th width="20">Memoria RAM(gb):</th>
          <th width="20">Disco (gb):</th>
          <th width="20">SO:</th>
          <th width="20">Base De Datos:</th>
        </tr>
        <?php
          $i=0;
          $sql="select CPU,Mem,SD,SDQ,SO,DB from crecmachine where No=".$_POST['sola'];
          $re = mysqli_query($conn,$sql);
          if(!$re)
            echo "Conexion con BD fallida";
          else
          {
            while($row = mysqli_fetch_array($re))
            {
              ?>
          <tr>
          <td><?php echo $i+1; if($i==0)$som=$row['SDQ']; ?></td>
          <td><input type="text" readonly="readonly" size="3" <?php echo "value=\"".$row['CPU']."\""; if($row['CPU']==""||$row['CPU']==0) echo "style=\"background:#85807d\""; ?>></td>
          <td><input type="text" readonly="readonly" size="3" <?php echo "value=\"".$row['Mem']."\""; if($row['Mem']==""||$row['Mem']==0) echo "style=\"background:#85807d\""; ?>></td>
          <td><input type="text" readonly="readonly" size="3" <?php echo "value=\"".$row['SD']."\"";  if($row['SD']==""||$row['SD']==0) echo "style=\"background:#85807d\"";?>></td>
          <td>
          <select style="width:100px"<?php if($row['SO']=="") echo " style=\"background:#85807d\""; ?>>
            <?php echo "<option value=\"".$row['SO']."\">".$row['SO']."</option>"; ?>
          </select>
          </td>
          <td>
          <select style="width:100px"<?php if($row['DB']=="") echo " style=\"background:#85807d\""; ?>>
            <?php echo "<option value=\"".$row['DB']."\">".$row['DB']."</option>"; ?>
          </select>
          </td>
        </tr>
        <?php
              $i++;
            }
          }
        ?>
      </table>
      <?php
        echo "<br><div style=\"margin-left: 50%;font-size:18px;\"> Disco compartido (spec):<input type=\"number\" readonly=\"readonly\" value=\"".$som."\"></div>";
      ?>
       <h1 align="center">Estado actual</h1>
      <table align="center">
        <tr>
          <th>No:</th>
          <th width="20">vCPUs:</th>
          <th width="20">Memoria RAM(gb):</th>
          <th width="20">Disco (gb):</th>
          <th width="20">SO:</th>
          <th width="20">Base De Datos:</th>
        </tr>
        <?php
          $j=0;
          $sql="select SVCPU,SRAM,SSto,SSO,SDB from maquinas where id=".$_POST['sola'];
          $re = mysqli_query($conn,$sql);
          if(!$re)
            echo "Conexion con BD fallida";
          else
          {
            while($row = mysqli_fetch_array($re))
            {
              ?>
          <tr>
          <td><?php echo $j+1;?></td>
          <td><input type="text" readonly="readonly" size="3" <?php echo "value=\"".$row['SVCPU']."\""; if($row['SVCPU']==""||$row['SVCPU']==0) echo "style=\"background:#85807d\""; ?>></td>
          <td><input type="text" readonly="readonly" size="3" <?php echo "value=\"".$row['SRAM']."\""; if($row['SRAM']==""||$row['SRAM']==0) echo "style=\"background:#85807d\""; ?>></td>
          <td><input type="text" readonly="readonly" size="3" <?php echo "value=\"".$row['SSto']."\"";  if($row['SSto']==""||$row['SSto']==0) echo "style=\"background:#85807d\"";?>></td>
          <td>
          <select style="width:100px"<?php if($row['SSO']=="") echo " style=\"background:#85807d\""; ?>>
            <?php echo "<option value=\"".$row['SSO']."\">".$row['SSO']."</option>"; ?>
          </select>
          </td>
          <td>
          <select style="width:100px"<?php if($row['SDB']=="") echo " style=\"background:#85807d\""; ?>>
            <?php echo "<option value=\"".$row['SDB']."\">".$row['SDB']."</option>"; ?>
          </select>
          </td>
        </tr>
        <?php
              $j++;
            }
          }
        ?>
      </table>
      <?php
       $sql="select SDQ from extras where id=".$_POST['sola'];
       $re2 = mysqli_query($conn,$sql);
       $row2 = mysqli_fetch_array($re2);
        echo "<br><div style=\"margin-left: 50%;font-size:18px;\"> Disco compartido (spec):<input type=\"text\" readonly=\"readonly\" value=\"".$row2['SDQ']."\"></div>";
      ?>
      <?php   mysqli_close($conn); ?>
      &ensp;&ensp;&ensp;&ensp;&ensp;
      <button type="button" class="evilbtn">Tecnologias Cloud</button>
      <br><br><br><br><br>
    </form>
    <?php echo "<form action='http://".$sols."'>"?>
    <div style="margin-left: 20%;font-size:18px;" >
    <input type="submit" value="Regresar" >
    </div>
  </div>
  </body>
</html>